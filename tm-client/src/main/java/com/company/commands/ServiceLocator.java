package com.company.commands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.endpoint.*;


import java.util.List;

public interface ServiceLocator {

    void setUser(User user);
    User getUser();
    List<AbstractCommand> getCommands();
    public boolean isValidDate(String date);
    void setSession(Session session);
    Session getSession();
    ProjectEndPointService getProjectEndPointService();
    TaskEndPointService getTaskEndPointService();
    UserEndPointService getUserEndPointService();
    TerminalServiceImpl getTerminalService();
    DataEndPointService getDataEndPointService();
    SessionEndPointService getSessionEndPointService();

}
