package com.company.commands.userCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.User;
import com.company.endpoint.UserEndPointService;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-newPassword";
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();

        System.out.println("ENTER NEW PASSWORD");
        String password = terminalService.nextLine();
        if (password.isEmpty()){
            throw new EmptyField();
        }
        password = userServiceImpl.getUserEndPointPort().generateMD5(password);
        final User user = serviceLocator.getUser();
        user.setPassword(password);
        serviceLocator.setUser(user);
        userServiceImpl.getUserEndPointPort().userUpdate(serviceLocator.getSession(), serviceLocator.getUser());
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserUpdatePasswordCommand(ServiceLocator bootstrap) {
       setServiceLocator(bootstrap);
    }

}
