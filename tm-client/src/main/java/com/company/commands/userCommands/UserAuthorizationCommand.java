package com.company.commands.userCommands;

import com.company.commands.ServiceLocator;
import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.SessionEndPointService;
import com.company.endpoint.UserEndPointService;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserAuthorizationCommand extends AbstractCommand  {

    public String command() {
        return "user-authorization";
    }


    public String description() {
        return "Authorization";
    }


    public void execute() throws Exception {
        @NotNull
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        @NotNull
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();
        @NotNull
        final SessionEndPointService sessionService = serviceLocator.getSessionEndPointService();
        System.out.println("ENTER LOGIN");
        String login = terminalService.nextLine();
        if(login.isEmpty()){
            throw new EmptyField();
        }
        System.out.println("ENTER PASSWORD");
        String password = terminalService.nextLine();
        if(password.isEmpty()){
            throw new EmptyField();
        }
        password = userServiceImpl.getUserEndPointPort().generateMD5(password);
        if((userServiceImpl.getUserEndPointPort().userRead(login,password))==null){
            System.out.println("USER NOT FOUND");
            System.out.println("Registration:");
            System.out.println("ENTER LOGIN");
            login = terminalService.nextLine();
            System.out.println("ENTER PASSWORD");
            password = userServiceImpl.getUserEndPointPort().generateMD5(terminalService.nextLine());
            userServiceImpl.getUserEndPointPort().registration(login,password);
        }
        else {
            System.out.println("Authorization success");
            serviceLocator.setUser(userServiceImpl.getUserEndPointPort().userRead(login,password));
            serviceLocator.setSession(sessionService.getSessionEndPointPort().openSession(serviceLocator.getUser()));
        }

    }
    public boolean secureCommand() {
        return false;
    }


    public UserAuthorizationCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }

}
