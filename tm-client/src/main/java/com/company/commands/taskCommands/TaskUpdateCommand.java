package com.company.commands.taskCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.Status;
import com.company.endpoint.Task;
import com.company.endpoint.TaskEndPointService;
import com.company.exception.EmptyField;
import com.company.exception.NoPermission;
import com.company.exception.WrongDateFormat;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor

public final class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();

        System.out.println("ENTER ID");

        String line = terminalService.nextLine();
            if (taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                Task taskPool = taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line);
                System.out.println("ENTER NEW NAME");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                taskPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
                final String dateStart = terminalService.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE TASK");
                final String dateEnd = terminalService.nextLine().trim();
                if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
                    throw new WrongDateFormat();

                }
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                Date dateEnd2 = dateFormatter.parse(dateEnd);
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(dateEnd2);
                XMLGregorianCalendar xmlDateEnd = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                Date dateStart2 = dateFormatter.parse(dateStart);
                cal.setTime(dateStart2);
                XMLGregorianCalendar xmlDateStart = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                taskPool.setDateBegin(xmlDateEnd);
                taskPool.setDateEnd(xmlDateStart);
                System.out.println("ENTER THE STATUS OF THE TASK");
                System.out.println("1 - Pending, 2 - Running, 3 - Finished");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                switch (line){
                    case "1":
                        taskPool.setDisplayName(Status.PENDING);
                        break;
                    case "2":
                        taskPool.setDisplayName(Status.RUNNING);
                        break;
                    case "3":
                        taskPool.setDisplayName(Status.FINISHED);
                        break;
                }
                taskServiceImpl.getTaskEndPointPort().taskUpdate(serviceLocator.getSession(), taskPool);
                System.out.println("SUCCESS");
            } else {
                throw new NoPermission();

            }

    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
