package com.company.commands.taskCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.endpoint.TaskEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();

        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        taskServiceImpl.getTaskEndPointPort().taskClear(serviceLocator.getSession());
        }
        else {
            throw new NoPermission();
        }
        }

    public boolean secureCommand() {
        return true;
    }

    public TaskClearCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }

}
