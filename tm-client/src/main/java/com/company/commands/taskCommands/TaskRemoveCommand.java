package com.company.commands.taskCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.TaskEndPointService;
import com.company.exception.NoPermission;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor


public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();

        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        if(taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getUserId().equals(serviceLocator.getUser().getUserId())){
        taskServiceImpl.getTaskEndPointPort().taskRemove(serviceLocator.getSession(), line);
        System.out.println("TASK REMOVED");}
        else {
            throw new NoPermission();

        }


    }
    public boolean secureCommand() {
        return true;
    }

    public TaskRemoveCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }


}
