package com.company.exception;

public class WrongDateFormat extends Exception{
    @Override
    public String toString() {
        return "Wrong date format. Should be dd.MM.yyyy";}

}
