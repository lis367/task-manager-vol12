package com.company.Interfaces;

import com.company.entity.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface TaskRepoInterface {

     void persist(Task task);  //create
     Task findOne(String id);  // read
     List findAll(String id); // readAll
     List<Task> findAll(); //readAll without ID
     void merge (Task task); //update
     void remove(String id); // delete
     void removeAll(); // deleteAll
     void saveInDB() throws Exception;
     void load() throws IOException, ClassNotFoundException;



}
