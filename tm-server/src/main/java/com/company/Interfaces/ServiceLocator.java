package com.company.Interfaces;


import com.company.entity.User;
import com.company.service.TerminalServiceImpl;

import java.sql.Connection;


public interface ServiceLocator {

    void setUser(User user);

    User getUser();

    public boolean isValidDate(String date);

    Connection getConnection();

    ProjectServiceInterface getProjectService();

    TaskServiceInterface getTaskService();

    UserServiceInterface getUserService();

    DataServiceInterface getDataService();

    SessionServiceInterface getSessionService();

    TerminalServiceImpl getTerminalService();


}
