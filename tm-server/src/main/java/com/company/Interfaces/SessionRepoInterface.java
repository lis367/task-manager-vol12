package com.company.Interfaces;

import com.company.entity.Session;
import com.company.exception.ObjectIsNotFound;

import java.util.List;

public interface SessionRepoInterface {

    Session findOne(String id) throws ObjectIsNotFound;
    List<Session> findAll();
    void persist(Session session);
    void merge(Session session);
    void remove(String id);
    void removeAll();

}
