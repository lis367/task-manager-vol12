package com.company.endpoint;

import com.company.Interfaces.DataServiceInterface;
import com.company.Interfaces.SessionServiceInterface;
import com.company.entity.Data;
import com.company.entity.Session;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class DataEndPoint {

    private DataServiceInterface dataService;
    private SessionServiceInterface sessionService;

    public DataEndPoint(DataServiceInterface dataService, SessionServiceInterface sessionService) {
        this.dataService = dataService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void load(@WebParam(name = "Session") Session session, @WebParam(name = "Data") Data data) throws Exception {
        sessionService.validate(session);
        try {
            dataService.load(data);
        } catch (EmptyField emptyField) {
            emptyField.printStackTrace();
        }
    }

    @WebMethod
    public Data save(@WebParam(name = "Session") Session session,@WebParam (name ="Data")final Data data) throws Exception {
        sessionService.validate(session);
        return dataService.save(data);

    }

}
