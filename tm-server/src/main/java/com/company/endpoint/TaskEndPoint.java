package com.company.endpoint;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.SessionServiceInterface;
import com.company.Interfaces.TaskServiceInterface;
import com.company.entity.Session;
import com.company.entity.Task;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import lombok.NoArgsConstructor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@WebService
public class TaskEndPoint {

    private ProjectServiceInterface projectService;
    private TaskServiceInterface taskService;
    private SessionServiceInterface sessionService;

    public TaskEndPoint(ProjectServiceInterface projectService, TaskServiceInterface taskService, SessionServiceInterface sessionService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public String taskCreate(
            @WebParam(name = "Session") Session session,
            @WebParam(name ="name")String name,
            @WebParam (name ="projectId")String projectId,
            @WebParam (name ="dateStart")Date dateStart,
            @WebParam (name ="dateEnd")Date dateEnd,
            @WebParam (name ="userId")String userId,
            @WebParam (name ="description") String description) throws Exception
    {   sessionService.validate(session);
        return taskService.taskCreate(name,projectId,dateStart,dateEnd,userId,description);
    }

    @WebMethod
    public Task taskRead (@WebParam(name = "Session") Session session, @WebParam (name ="Id")String id) throws Exception {
        sessionService.validate(session);
        return taskService.read(id);
    }

    @WebMethod
    public void taskUpdate(@WebParam(name = "Session") Session session,@WebParam(name ="Task") Task task) throws Exception {
        sessionService.validate(session);
        taskService.update(task);
    }
    @WebMethod
    public void taskRemove(@WebParam(name = "Session") Session session,
                           @WebParam (name ="userId")String id) throws Exception {
        sessionService.validate(session);
        taskService.taskRemove(id);
    }

    @WebMethod
    public List<Task> taskList(@WebParam(name = "Session") Session session,
                               @WebParam(name ="Id")String id) throws Exception {
        sessionService.validate(session);
        return taskService.taskList(id);
    }

    @WebMethod
    public void taskClear(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        taskService.taskClear();
    }

    @WebMethod
    public void setTasks(@WebParam(name = "Session") Session session,List<Task> tasks) throws Exception {
        sessionService.validate(session);
        taskService.setTasks(tasks);
    }

    @WebMethod
    public List<Task> allTaskList(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        return taskService.allTaskList();
    }

    @WebMethod
    public void taskLoad(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        try {
            taskService.load();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @WebMethod
    public void saveInDB(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        taskService.saveInDb();
    }



}
