package com.company.repository;


import com.company.Interfaces.UserRepoInterface;
import com.company.entity.*;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Getter
@Setter
public final class UserRepositoryImpl implements UserRepoInterface {

    @SneakyThrows
    public User findOne(String login, String password){
        User user = null;
        try {
            @NotNull final String query =
                    "SELECT * FROM app_user WHERE login =? AND passwordHash =?";
            @NotNull final PreparedStatement statement = new MySQLConnector().createConnection().prepareStatement(query);
            statement.setString(1, login);
            statement.setString(2, password);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) user = fetch(resultSet);
        } catch (NullPointerException e) {
            throw new ObjectIsNotFound();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }

    @SneakyThrows
    @Override
    public List<User> findAll() {
        @NotNull final String query =
                "SELECT * FROM app_task";
        @NotNull final PreparedStatement statement =
                new MySQLConnector().createConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        List<User> users = new ArrayList<User>();
        while (resultSet.next()) users.add(fetch(resultSet));
        statement.close();
        return users;
    }

    @SneakyThrows
    public void persist(User user)
    {
        User userPersist = findOne(user.getName(), user.getPassword());
        if (userPersist ==null){
            Connection connection = new MySQLConnector().createConnection();
            String query = "INSERT INTO app_user (id, login, passwordHash, role)"
                    + "VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,user.getUserId());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3,user.getPassword());
            preparedStatement.setString(4, String.valueOf(user.getUserRoleType()));
            preparedStatement.execute();
        }
        else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    @SneakyThrows
    public void merge(User user) {
        User userUpdate = findOne(user.getName(), user.getPassword());
        if(userUpdate ==null){
                throw new ObjectIsNotFound();
            }
        else {
            @NotNull final String query = "UPDATE app_user SET id=?, login=?, passwordHash=?, role=?" +
                    "WHERE id = ?";
            Connection connection = new MySQLConnector().createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,user.getUserId());
            preparedStatement.setString(2,user.getName());
            preparedStatement.setString(3,user.getPassword());
            preparedStatement.setString(4, String.valueOf(user.getUserRoleType()));
            preparedStatement.execute();
        }
    }

    @SneakyThrows
    @Override
    public void remove(User user) {
        @NotNull final String query = "DELETE FROM app_user WHERE id = ?";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,user.getUserId());
        preparedStatement.execute();
    }

    public void save() throws IOException {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/resources/user.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            List<User> arrayUser = findAll();
            for (User userEntry : arrayUser) {
                objectOutputStream.writeObject(userEntry);
            }
            objectOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void load() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/resources/user.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        boolean loop = true;
        while (loop){
            if(fileInputStream.available()!=0){
        User user = (User) objectInputStream.readObject();
        persist(user); }
        else {
            loop = false;
            }
        }
        fileInputStream.close();
        objectInputStream.close();
    }

    @SneakyThrows
    private User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setName(row.getString("login"));
        user.setPassword(row.getString("passwordHash"));
        user.setUserId(row.getString("id"));
        if(row.getString("role").equals(String.valueOf(UserRoleType.USER))){
           user.setUserRoleType(UserRoleType.USER);
        }
        else if(row.getString("role").equals(String.valueOf(UserRoleType.ADMIN))){
           user.setUserRoleType(UserRoleType.ADMIN);
        }
        return user;
    }

    public UserRepositoryImpl() {
    }
}
