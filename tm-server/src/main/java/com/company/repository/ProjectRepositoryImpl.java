package com.company.repository;

import com.company.Interfaces.ProjectRepoInterface;
import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.MySQLConnector;
import com.company.entity.Project;
import com.company.entity.Status;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Getter
@Setter

public final class ProjectRepositoryImpl implements ProjectRepoInterface {

    public Project findOne(String id) throws ObjectIsNotFound {
        Project project = null;
        try {
            @NotNull final String query =
                    "SELECT * FROM app_project WHERE id =?";
            @NotNull final PreparedStatement statement = new MySQLConnector().createConnection().prepareStatement(query);
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) project = fetch(resultSet);
        } catch (NullPointerException e) {
            throw new ObjectIsNotFound();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }

    @SneakyThrows
    public List<Project> findAll(String userID) {
        @NotNull final String query =
                "SELECT * FROM app_project WHERE user_id = ?";
        @NotNull final PreparedStatement statement =
                new MySQLConnector().createConnection().prepareStatement(query);
        statement.setString(1, userID);
       @NotNull final ResultSet resultSet = statement.executeQuery();
        List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        return projects;
    }

    @SneakyThrows
    @Override
    public List<Project> findAll() {
        @NotNull final String query =
                "SELECT * FROM app_project";
        @NotNull final PreparedStatement statement =
                new MySQLConnector().createConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        List<Project> projects = new ArrayList<Project>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        return projects;
    }

    @SneakyThrows
    public void persist(Project project) {
        Project projectPersist = findOne(project.getId());
        if (projectPersist == null) {
            Connection connection = new MySQLConnector().createConnection();
            String query = "INSERT INTO app_project (id, dateBegin, dateEnd, description, name, user_id, displayName, creationDate)"
                    + "VALUES (?, ?, ?, ?, ?, ?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,project.getId());
            java.sql.Date startDate = new java.sql.Date(project.getDateBegin().getTime());
            java.sql.Date endDate = new java.sql.Date(project.getDateEnd().getTime());
            preparedStatement.setDate(2,startDate);
            preparedStatement.setDate(3,endDate);
            preparedStatement.setString(4,project.getDescription());
            preparedStatement.setString(5,project.getName());
            preparedStatement.setString(6,project.getUserId());
            preparedStatement.setString(7, String.valueOf(project.getDisplayName()));
            java.sql.Date creationDate = new java.sql.Date(project.getCreationDate().getTime());
            preparedStatement.setDate(8,creationDate);
            preparedStatement.execute();
        } else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    @SneakyThrows
    public void merge(Project project) {
        Project projectPersist = findOne(project.getId());
        if (projectPersist == null){
            throw new ObjectIsNotFound();
        } else {
        @NotNull final String query = "UPDATE app_project SET id=?,datebegin=?,dateEnd=?,description=?,name=?,user_id=?,displayName=?,creationDate=? " +
                "WHERE id = ?";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,project.getId());
        java.sql.Date startDate = new java.sql.Date(project.getDateBegin().getTime());
        java.sql.Date endDate = new java.sql.Date(project.getDateEnd().getTime());
        preparedStatement.setDate(2,startDate);
        preparedStatement.setDate(3,endDate);
        preparedStatement.setString(4,project.getDescription());
        preparedStatement.setString(5,project.getName());
        preparedStatement.setString(6,project.getUserId());
        preparedStatement.setString(7, String.valueOf(project.getDisplayName()));
        java.sql.Date creationDate = new java.sql.Date(project.getCreationDate().getTime());
        preparedStatement.setDate(8,creationDate);
        preparedStatement.setString(9,project.getId());
        preparedStatement.execute();
        }
    }

    @SneakyThrows
    public void remove(String id) {
        @NotNull final String query = "DELETE FROM app_project WHERE id = ?";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,id);
        preparedStatement.execute();
        @NotNull final String taskQuery = "DELETE FROM app_task WHERE project_id = ?";
        preparedStatement = connection.prepareStatement(taskQuery);
        preparedStatement.setString(1,id);
        preparedStatement.execute();

    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final String query = "DELETE FROM app_project";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.execute();
        @NotNull final String taskQuery = "DELETE FROM app_task";
        preparedStatement=connection.prepareStatement(taskQuery);
        preparedStatement.execute();
    }

    //Сохранение в обход класса Дата
    @Override
    public void saveInDB() throws Exception {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/resources/projects.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            List<Project> projects = findAll();
            for (Project projectEntry : projects) {
                objectOutputStream.writeObject(projectEntry);
            }
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Загрузка в обход класса Дата
    @Override
    public void load() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/resources/projects.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        boolean loop = true;
        while (loop){
            if(fileInputStream.available()!=0){
                Project project = (Project) objectInputStream.readObject();
                persist(project);

        } else {
                loop = false;
            }
    }
        fileInputStream.close();
        objectInputStream.close();
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setDateBegin(row.getDate("dateBegin"));
        project.setDateEnd(row.getDate("dateEnd"));
        project.setCreationDate(row.getDate("creationDate"));
        project.setUserId(row.getString("user_id"));
        if(row.getString("displayName").equals(String.valueOf(Status.PENDING))){
        project.setDisplayName(Status.PENDING);
        }
        else if(row.getString("displayName").equals(String.valueOf(Status.RUNNING))){
            project.setDisplayName(Status.RUNNING);
        }
        else if(row.getString("displayName").equals(String.valueOf(Status.FINISHED))){
            project.setDisplayName(Status.FINISHED);
        }
        return project;
    }


    public ProjectRepositoryImpl() {
    }
}
