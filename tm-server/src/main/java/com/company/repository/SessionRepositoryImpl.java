package com.company.repository;

import com.company.Interfaces.SessionRepoInterface;
import com.company.entity.*;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SessionRepositoryImpl implements SessionRepoInterface {

    @SneakyThrows
    public Session findOne(String id) throws ObjectIsNotFound {
        Session session = null;
        try {
            final String query =
                    "SELECT * FROM app_session WHERE user_id =?";
            @NotNull final PreparedStatement statement = new MySQLConnector().createConnection().prepareStatement(query);
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) session = fetch(resultSet);
        } catch (NullPointerException e) {
            throw new ObjectIsNotFound();
        }
        return session;
    }

    @SneakyThrows
    public List<Session> findAll(){
        @NotNull final String query =
                "SELECT * FROM app_session";
        @NotNull final PreparedStatement statement =
                new MySQLConnector().createConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        List<Session> sessions = new ArrayList<Session>();
        while (resultSet.next()) sessions.add(fetch(resultSet));
        statement.close();
        return sessions;
    }

    @SneakyThrows
    public void persist(Session session){
        Session session1 = findOne(session.getId());
        if (session1 == null) {
            Connection connection = new MySQLConnector().createConnection();
            String query = "INSERT INTO app_session (id, signature, timestamp, user_id)"
                    + "VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,session.getId());
            preparedStatement.setString(2,session.getSignature());
            preparedStatement.setLong(3,session.getTimestamp());
            preparedStatement.setString(4,session.getUserId());
            preparedStatement.execute();

        } else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    @SneakyThrows
    public void merge(Session session) {
        Session session1 = findOne(session.getId());
        if (session1==null) throw new ObjectIsNotFound();
        else {
            @NotNull final String query = "UPDATE app_session SET id=?, login=?, passwordHash=?, role=?" +
                    "WHERE id = ?";
            Connection connection = new MySQLConnector().createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,session.getId());
            preparedStatement.setString(2,session.getSignature());
            preparedStatement.setLong(3,session.getTimestamp());
            preparedStatement.setString(4,session.getUserId());
            preparedStatement.execute();
        }
    }

    @SneakyThrows
    public void remove(String id) {
        @NotNull final String query = "DELETE FROM app_session WHERE id = ?";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,id);
        preparedStatement.execute();

    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final String taskQuery = "DELETE FROM app_session";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(taskQuery);
        preparedStatement.execute();
    }

    @SneakyThrows
    private Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setSignature(row.getString("signature"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setUserId(row.getString("user_id"));
        return session;
    }

}
