package com.company.repository;

import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.MySQLConnector;
import com.company.entity.Project;
import com.company.entity.Status;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Getter
@Setter
public final class TaskRepositoryImpl implements TaskRepoInterface {

    @SneakyThrows
    public void persist(Task task)  {
        Task taskPersist = findOne(task.getId());
        if (taskPersist == null) {
            Connection connection = new MySQLConnector().createConnection();
            String query = "INSERT INTO app_task (id, dateBegin, dateEnd, description, name, project_id, user_id, displayName, creationDate)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,task.getId());
            java.sql.Date startDate = new java.sql.Date(task.getDateBegin().getTime());
            java.sql.Date endDate = new java.sql.Date(task.getDateEnd().getTime());
            preparedStatement.setDate(2,startDate);
            preparedStatement.setDate(3,endDate);
            preparedStatement.setString(4,task.getDescription());
            preparedStatement.setString(5,task.getName());
            preparedStatement.setString(6,task.getProjectID());
            preparedStatement.setString(7,task.getUserId());
            preparedStatement.setString(8, String.valueOf(task.getDisplayName()));
            java.sql.Date creationDate = new java.sql.Date(task.getCreationDate().getTime());
            preparedStatement.setDate(9,creationDate);
            preparedStatement.execute();
        } else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    @SneakyThrows
    public Task findOne(String id){
        Task task = null;
        try {
            @NotNull final String query =
                    "SELECT * FROM app_task WHERE id =?";
            @NotNull final PreparedStatement statement = new MySQLConnector().createConnection().prepareStatement(query);
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) task = fetch(resultSet);
        } catch (NullPointerException e) {
            throw new ObjectIsNotFound();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return task;
    }

    @SneakyThrows
    public List<Task> findAll(String userID){
        @NotNull final String query =
                "SELECT * FROM app_task WHERE user_id = ?";
        @NotNull final PreparedStatement statement =
                new MySQLConnector().createConnection().prepareStatement(query);
        statement.setString(1, userID);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        return tasks;
    }

    @SneakyThrows
    @Override
    public List<Task> findAll() {
        @NotNull final String query =
                "SELECT * FROM app_task";
        @NotNull final PreparedStatement statement =
                new MySQLConnector().createConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        List<Task> tasks = new ArrayList<Task>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        return tasks;
    }

    @SneakyThrows
    public void merge (Task task)
    {   Task taskPersist = findOne(task.getId());
        if (taskPersist == null) {
            throw new ObjectIsNotFound();
        } else{
        @NotNull final String query = "UPDATE app_task SET id=?,datebegin=?,dateEnd=?,description=?,name=?,project_id=?,user_id=?,displayName=?,creationDate=? " +
            "WHERE id = ?";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,task.getId());
        java.sql.Date startDate = new java.sql.Date(task.getDateBegin().getTime());
        java.sql.Date endDate = new java.sql.Date(task.getDateEnd().getTime());
        preparedStatement.setDate(2,startDate);
        preparedStatement.setDate(3,endDate);
        preparedStatement.setString(4,task.getDescription());
        preparedStatement.setString(5,task.getName());
        preparedStatement.setString(6,task.getProjectID());
        preparedStatement.setString(7,task.getUserId());
        preparedStatement.setString(8, String.valueOf(task.getDisplayName()));
        java.sql.Date creationDate = new java.sql.Date(task.getCreationDate().getTime());
        preparedStatement.setDate(9,creationDate);
        preparedStatement.setString(10,task.getId());
        preparedStatement.execute();
    }
    }

    @SneakyThrows
    public void remove(String id){
        @NotNull final String query = "DELETE FROM app_task WHERE id = ?";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,id);
        preparedStatement.execute();
    }

    @SneakyThrows
    public void removeAll(){
        @NotNull final String taskQuery = "DELETE FROM app_task";
        Connection connection = new MySQLConnector().createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(taskQuery);
        preparedStatement.execute();
    }

    @Override
    public void saveInDB() throws Exception {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/resources/tasks.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            List<Task> tasks = findAll();
            for (Task taskEntry : tasks) {
                objectOutputStream.writeObject(taskEntry);
            }
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/resources/tasks.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        boolean loop = true;
        while (loop){
            if(fileInputStream.available()!=0){
                Task task = (Task) objectInputStream.readObject();
              persist(task);
        }
            else {
                loop = false;
            }
    }
        fileInputStream.close();
        objectInputStream.close();
    }

    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setDateBegin(row.getDate("dateBegin"));
        task.setDateEnd(row.getDate("dateEnd"));
        task.setCreationDate(row.getDate("creationDate"));
        task.setUserId(row.getString("user_id"));
        task.setProjectID(row.getString("project_id"));
        if(row.getString("displayName").equals(String.valueOf(Status.PENDING))){
            task.setDisplayName(Status.PENDING);
        }
        else if(row.getString("displayName").equals(String.valueOf(Status.RUNNING))){
            task.setDisplayName(Status.RUNNING);
        }
        else if(row.getString("displayName").equals(String.valueOf(Status.FINISHED))){
            task.setDisplayName(Status.FINISHED);
        }
        return task;
    }

    public TaskRepositoryImpl() {
    }
}

