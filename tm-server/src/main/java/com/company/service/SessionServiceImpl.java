package com.company.service;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.SessionRepoInterface;
import com.company.Interfaces.SessionServiceInterface;
import com.company.entity.Session;
import com.company.entity.SignatureUtil;
import com.company.entity.User;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import com.company.repository.SessionRepositoryImpl;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor

@Getter
@Setter
public class SessionServiceImpl implements SessionServiceInterface {

    private SessionRepoInterface sessionRepo = new SessionRepositoryImpl();
    private ServiceLocator bootstrap;


    public void closeSession(Session session) throws EmptyField {
        validate(session);
        sessionRepo.remove(session.getId());
    }

    public List getListSession(){
        return sessionRepo.findAll();
    }

    public User getUser(){
        return bootstrap.getUser();
    }

    @SneakyThrows
    public Session openSession(User user){
        Session session =sessionRepo.findOne(user.getUserId());
        if (session ==null){
            session = new Session();
        session = sign(session);
        session.setTimestamp(24*60*60L);
        session.setUserId(user.getUserId());
        sessionRepo.persist(session);}
        return session;
    }

    public void validate(Session session) throws EmptyField {
        if(session==null||session.getSignature().isEmpty()||session.getUserId().isEmpty()||session.getTimestamp()==null){
            throw new EmptyField();
        }
        try {
            if(sessionRepo.findOne(session.getUserId())==null) throw new EmptyField();
        } catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();
        }
    }

    public Session sign(@Nullable final Session session){
        if(session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = SignatureUtil.sign(session, "salt", 2);
        session.setSignature(signature);
        return session;
    }


    public SessionServiceImpl(ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }
}
