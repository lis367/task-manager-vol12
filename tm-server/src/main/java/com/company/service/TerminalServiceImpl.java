package com.company.service;

import java.util.Scanner;

public class TerminalServiceImpl {

    public String nextLine(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

}
