package com.company.service;

import com.company.Interfaces.*;
import com.company.entity.Data;
import com.company.exception.EmptyField;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;


public class DataService implements DataServiceInterface {


    @NotNull
    private ServiceLocator serviceLocator;


    @Override
    public void load(@Nullable Data data) {
        final ProjectServiceInterface projectService = serviceLocator.getProjectService();
        final TaskServiceInterface taskService = serviceLocator.getTaskService();
        final UserServiceInterface userService = serviceLocator.getUserService();
        try{
        if(data.getProjects()==null){
            projectService.setProjects(new ArrayList<>());
        } else projectService.setProjects(data.getProjects());

        if(data.getTasks()==null){
            taskService.setTasks(new ArrayList<>());
        } else taskService.setTasks(data.getTasks());

        if(data.getUsers()==null){
            userService.setUsers(new ArrayList<>());
        } else userService.setUsers(data.getUsers());}
        catch (EmptyField e){
            e.printStackTrace();
        }



    }

    @Override
    public @Nullable Data save(@Nullable final Data data) {
        if (data == null) return data;
        final ProjectServiceInterface projectService = serviceLocator.getProjectService();
        final TaskServiceInterface taskService = serviceLocator.getTaskService();
        final UserServiceInterface userService = serviceLocator.getUserService();

        data.setProjects(projectService.allProjectList());
        data.setTasks(taskService.allTaskList());
        data.setUsers(userService.allUserList());
        return data;
    }

    public DataService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
