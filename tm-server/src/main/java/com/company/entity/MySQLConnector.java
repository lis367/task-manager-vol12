package com.company.entity;

import com.mysql.fabric.jdbc.FabricMySQLDriver;
import lombok.SneakyThrows;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLConnector {

    @SneakyThrows
    public Connection createConnection()  {
        Properties property = new Properties();
        FileInputStream fis = new FileInputStream("tm-server/src/main/resources/config.properties");
        property.load(fis);
        final String URL = property.getProperty("db.host");
        final String USERNAME = property.getProperty("db.login");
        final String PASSWORD = property.getProperty("db.password");
        Connection connection = null;
        try{
        Driver driver = new FabricMySQLDriver();
        DriverManager.registerDriver(driver);
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        if (!connection.isClosed()) {
            System.out.println("СОЕДИНЕНИЕ с MYSQL Успешное");
        }}
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
