package com.company.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor

public final class Project implements Serializable {
    @NotNull
    private String name;
    @NotNull
    private String id;
    @Nullable
    private String description;
    @NotNull
    private Date dateBegin;
    @NotNull
    private Date dateEnd;
    @NotNull
    private String userId;
    @NotNull
    private Status displayName;
    @NotNull
    private Date creationDate;
    private static final long serialVersionUID = 1L;


    public Project(String name, Date start, Date end, String id, String userId) {
        this.name = name;
        this.dateBegin = start;
        this.dateEnd = end;
        this.id = id;
        this.userId = userId;
    }

}
