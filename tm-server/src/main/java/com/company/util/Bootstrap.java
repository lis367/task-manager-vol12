package com.company.util;

import com.company.Interfaces.*;
import com.company.endpoint.*;
import com.company.entity.*;
import com.company.service.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private ProjectServiceInterface projectService = new ProjectServiceImpl(this);
    @NotNull
    private TaskServiceInterface taskService = new TaskServiceImpl(this);
    @NotNull
    private UserServiceInterface userService = new UserServiceImpl();
    @NotNull
    private DataServiceInterface dataService = new DataService(this);
    @NotNull
    private SessionServiceInterface sessionService = new SessionServiceImpl(this);
    @NotNull
    private TerminalServiceImpl terminalService = new TerminalServiceImpl();
    @Nullable
    private User user;

    private Connection connection = new MySQLConnector().createConnection();


    public void start() throws Exception {
        serviceAndRepoInit();
        System.out.println("*** SERVER IS RUNNING ***");

    }
    // Созданы Эндпоинты инициализирован пользователь админ
    public void serviceAndRepoInit(){
        try {
            userService.save(initAdmin());
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ProjectEndPoint projectEndPoint = new ProjectEndPoint(projectService,sessionService);
        final TaskEndPoint taskEndPoint = new TaskEndPoint(projectService, taskService, sessionService);
        final UserEndPoint userEndPoint = new UserEndPoint(userService,sessionService);
        final DataEndPoint dataEndPoint = new DataEndPoint(dataService,sessionService);
        final SessionEndPoint sessionEndPoint= new SessionEndPoint(sessionService);

        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectEndPoint);
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskEndPoint);
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userEndPoint);
        Endpoint.publish("http://localhost:8080/DataEndpoint?wsdl", dataEndPoint);
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",sessionEndPoint);


    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
    public User initAdmin(){
        User admin = new User();
        admin.setName("admin");
        admin.setPassword(PasswordHashUtil.md5("admin"));
        admin.setUserRoleType(UserRoleType.ADMIN);
        admin.setUserId(UUID.randomUUID().toString());
        return admin;
    }

    public ProjectServiceInterface getProjectService() {
        return projectService;
    }

    public TaskServiceInterface getTaskService() {
        return taskService;
    }

    public UserServiceInterface getUserService() {
        return userService;
    }

    public DataServiceInterface getDataService() {
        return dataService;
    }

    public SessionServiceInterface getSessionService() {
        return sessionService;
    }

    public TerminalServiceImpl getTerminalService() {
        return terminalService;
    }

    @Override
    public Connection getConnection() {
        return connection;
    }
}

